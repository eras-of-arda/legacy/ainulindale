Welcome to the Ainulindalë API repository. Ainulindalë is a third party addon API for LOTR mod by Mevans.


__Instalation__

All downloads can be found in the the [release tab](https://gitlab.com/eras-of-arda/ainulindale/-/releases "Go to release tab").

Ainulindalë can be installed like any other Forge mod, just put the mod in your mods folder and it will do it's thing. For it to have any effect you need [LOTR Mod](https://lotrminecraftmod.fandom.com/wiki/The_Lord_of_the_Rings_Minecraft_Mod_Wiki "LOTR Mod wiki page.") by Mevans..

This mod is a Forge mod which runs on minecraft 1.7.10.


__For Developers__

Any developer can use this API for their mod, as long as it's kept as a separate jar and they provide credits. Information on how to use the mod can be found on the wiki (TODO) or by reading the java docs. Normally you would only need to interact with the classes in the API package but some of the other packages may also contain useful helper classes.


__Testing__

When running the mod from a IDE make sure to add `-Dfml.coreMods.load=eoa.ainulindale.core.AinulindaleCoreMod` to the VM arguments. If you don't do this the core mod will not load and the mod will not work propperly.
