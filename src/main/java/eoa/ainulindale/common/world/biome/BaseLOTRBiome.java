package eoa.ainulindale.common.world.biome;

import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.BiomeRegistry;
import eoa.ainulindale.common.Ainulindale;
import lotr.common.LOTRDimension;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import net.minecraft.block.Block;
import net.minecraft.world.World;

import java.util.Random;

public abstract class BaseLOTRBiome extends LOTRBiome {

    private BaseLOTRBiome(String addonID, boolean major) {
        this(addonID, major, LOTRDimension.MIDDLE_EARTH);
    }

    private BaseLOTRBiome(String addonID, boolean major, LOTRDimension dim) {
        super(BiomeRegistry.getNextBiome(addonID, null), major, dim);
        Ainulindale.log.info("Registering biome (unnamed) with ID "+this.biomeID);
    }

    public BaseLOTRBiome(String addonID, boolean major, int color, String name, AinulindaleBiomeProperties props) {
        this(addonID, major, LOTRDimension.MIDDLE_EARTH, color, name, props);
    }

    public BaseLOTRBiome(String addonID, boolean major, LOTRDimension dim, int color, String name, AinulindaleBiomeProperties props) {
        super(BiomeRegistry.getNextBiome(addonID, color), major, dim);
        Ainulindale.log.info("Registering biome "+addonID+":"+name+" with ID "+this.biomeID);
        if(!BiomeRegistry.getBiomeColorList(addonID).containsKey(color | -16777216))
        {
            Ainulindale.log.info("Overriding color list for biome");
            LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.remove(color | -16777216);
        }
        this.setColor(color);
        this.setBiomeName(addonID+":"+name);
        this.setMinMaxHeight(props.minHeight, props.maxHeight);
        this.setTemperatureRainfall(props.temp, props.rain);
        this.setBanditChance(props.banditChance);
        BiomeRegistry.setBiomeEntry(addonID, this.biomeID, this, color);
    }


    }
