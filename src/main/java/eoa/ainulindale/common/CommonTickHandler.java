package eoa.ainulindale.common;

import cpw.mods.fml.common.FMLCommonHandler;

public class CommonTickHandler {
    
    public CommonTickHandler() {
        FMLCommonHandler.instance().bus().register(this);
    }
}
