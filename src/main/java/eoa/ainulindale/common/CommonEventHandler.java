package eoa.ainulindale.common;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.FactionRegistry;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.common.network.AinulindaleNetworkHandler;
import eoa.ainulindale.common.network.S2C.PacketSetMapType;
import lotr.common.LOTRLevelData;
import lotr.common.LOTRPlayerData;
import lotr.common.fac.LOTRFaction;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;

public class CommonEventHandler {
    
    public CommonEventHandler() {
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);
    }

    @SubscribeEvent
    public void playerLogin(PlayerLoggedInEvent event) {
        EntityPlayer player = event.player;
        
        if(!player.worldObj.isRemote) {
            AinulindaleNetworkHandler.networkWrapper.sendTo(new PacketSetMapType(AddonRegistry.getActiveAddon()), (EntityPlayerMP) player);
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGH)
    public void onPlayerLoginFactionCheck(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayer player = event.player;

        if(!player.worldObj.isRemote) {
            LOTRPlayerData pd = LOTRLevelData.getData(player);

            if(pd.getViewingFaction() == LOTRFaction.HOBBIT) pd.setViewingFaction(FactionRegistry.getFactionList(AddonRegistry.getActiveAddon()).get(0));
        }
    }
}
