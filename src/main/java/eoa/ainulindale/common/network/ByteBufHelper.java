package eoa.ainulindale.common.network;

import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;

public class ByteBufHelper {
    
    public static void writeString(ByteBuf data, String s) {
        if(s == null) {
            data.writeShort(0);
            return;
        }
        
        byte[] bytes = s.getBytes(Charsets.UTF_8);
        data.writeShort(bytes.length);
        data.writeBytes(bytes);
    }
    
    public static String readString(ByteBuf data) {
        if(data.readableBytes() == 0) return null; 
        
        short length = data.readShort();
        if(length != 0) {
            return data.readBytes(length).toString(Charsets.UTF_8);
        }
        
        return null;
    }

}
