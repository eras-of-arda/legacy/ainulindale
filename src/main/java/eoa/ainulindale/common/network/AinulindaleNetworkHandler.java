package eoa.ainulindale.common.network;

import java.util.concurrent.atomic.AtomicInteger;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.network.C2S.PacketRequestMapKick;
import eoa.ainulindale.common.network.S2C.PacketReloadHiddenBiomes;
import eoa.ainulindale.common.network.S2C.PacketSetMapType;

public class AinulindaleNetworkHandler {
    public static final SimpleNetworkWrapper networkWrapper = NetworkRegistry.INSTANCE.newSimpleChannel("Ainulindale");

    public AinulindaleNetworkHandler() {
        AtomicInteger id = new AtomicInteger();

        networkWrapper.registerMessage(PacketReloadHiddenBiomes.Handler.class, PacketReloadHiddenBiomes.class, id.getAndIncrement(), Side.CLIENT);
        networkWrapper.registerMessage(PacketSetMapType.Handler.class, PacketSetMapType.class, id.getAndIncrement(), Side.CLIENT);
        
        networkWrapper.registerMessage(PacketRequestMapKick.Handler.class, PacketRequestMapKick.class, id.getAndIncrement(), Side.SERVER);

        Ainulindale.log.info("Registered " + id + " packet types.");
    }
}
