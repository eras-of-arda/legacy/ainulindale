package eoa.ainulindale.common.config.json;

import java.io.File;
import java.io.IOException;

import eoa.ainulindale.api.AinulindaleAddon;
import eoa.ainulindale.api.registries.AddonRegistry;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.LOTRShields;
import lotr.common.LOTRTitle;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.map.LOTRMountains;
import lotr.common.world.map.LOTRRoads;
import lotr.common.world.map.LOTRWaypoint;
import org.apache.commons.io.FileUtils;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.api.registries.MapRegistry.FileMapInfo;
import eoa.ainulindale.api.registries.MapRegistry.MapInfo;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.util.resources.ResourceHelper;
import net.minecraft.util.ResourceLocation;

public class WorldConfig extends AinulindaleAddon {
    // Json configs
    public ConfigWaypointRegions waypointRegionConfig;
    public ConfigWaypoints waypointConfig;
    
    // Main custom world folder
    public File customWorldFolder;
    
    // Custom map
    private ResourceLocation mapID;
    
    public WorldConfig(File customWorldFolder) {
        if(!customWorldFolder.exists()) customWorldFolder.mkdirs();
        this.customWorldFolder = customWorldFolder;
    }
    
    public void initializeConfigsPreInit() {
        try {
            FileUtils.copyInputStreamToFile(ResourceHelper.getInputStream(new ResourceLocation(Ainulindale.MODID, "config/readMe.txt")), new File(customWorldFolder, "readMe.txt"));
        }
        catch(IOException e) {
            Ainulindale.log.error("Unable to create readme file for the custom world config.");
            e.printStackTrace();
        }

        String jsonAddonID = "Data_"+customWorldFolder.getName();

        waypointRegionConfig = new ConfigWaypointRegions(this, jsonAddonID);
        waypointConfig = new ConfigWaypoints(this, jsonAddonID);
        
        File mapFile = new File(customWorldFolder, "map.png");

        MapInfo info;
        if(mapFile.exists())
        {
            info = new FileMapInfo(mapFile);
        }
        else
        {
            info = new MapRegistry.SimpleMapInfo(AddonRegistry.getMapIDForAddon("lotr")); //use vanilla LOTR map
        }

        //AddonRegistry.registerAddon("Data_"+customWorldFolder.getName()); //automatic naming scheme for data file addons until I add proper JSON config
        //MapInfo info = new FileMapInfo(mapFile);
        //MapRegistry.registerMap(info);


        //mapID = info.getID();

        //MapRegistry.setActiveMap(mapID);
        //AddonRegistry.setAddonForMapID(mapID, "Data_"+customWorldFolder.getName(), info);

        setupAddonPreInit(jsonAddonID, true, true, true, true, true, true, info, new LOTRFaction[]{}, new LOTRBiome[]{}, new LOTRWaypoint[]{}, new LOTRDimension.DimensionRegion[]{}, new LOTRMountains[]{}, new LOTRShields[]{});


        waypointRegionConfig.processJson();
        waypointConfig.processJson();

    }

    public void initializeConfigsPostInit() {
        String jsonAddonID = "Data_"+customWorldFolder.getName();
        setupAddonPostInit(jsonAddonID, true, true, true, true, true, new LOTRAchievement[]{}, new LOTRAchievement.Category[]{}, new LOTRMapLabels[]{}, new LOTRTitle[]{}, new LOTRRoads[]{});
    }

}
