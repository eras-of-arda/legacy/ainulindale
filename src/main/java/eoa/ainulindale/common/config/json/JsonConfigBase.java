package eoa.ainulindale.common.config.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.util.resources.ResourceHelper;
import net.minecraft.util.ResourceLocation;

public abstract class JsonConfigBase<E> {
    public String NAME_SPACE;
    private String configName;
    public File configFile; 
    

    public JsonConfigBase(String configName, WorldConfig config, String addonID) {
        this.configName = configName;
        this.NAME_SPACE = addonID;
        configFile = new File(config.customWorldFolder, configName + ".json");
        
        init();
    }
    
    public void init() {
        FileReader reader = null;
        
        try {
            if(!configFile.exists()) {
                configFile.createNewFile();
                FileUtils.copyInputStreamToFile(ResourceHelper.getInputStream(new ResourceLocation(Ainulindale.MODID, "config/" + configName + ".json")), configFile);
                
                return;
            }
            
            if(processOnLoad()) processJson();
        }
        catch(IOException e) {
            Ainulindale.log.error("Unable to create or read the " + configName + " json file.");
            e.printStackTrace();
        }
        catch(JsonParseException e) {
            Ainulindale.log.error("The " + configName + " json file has a invalid syntax.");
            e.printStackTrace();
        }
        catch(ClassCastException e) {
            Ainulindale.log.error("The " + configName + "  json file is in the incorrect type format. You can delete the file or have a look at the wiki to find the correct type format.");
            e.printStackTrace();
        }
        finally {
            IOUtils.closeQuietly(reader);
        }
    }
    
    public boolean processOnLoad() {
        return false;
    }
    
    public void processJson() {
        FileReader reader = null;
        try {
            reader = new FileReader(configFile);
            
            // Getting the type token from the generic type E (new TypeToken<E>() {}.getType()) doesn't work as generic types don't contain it's own generic type data or something so we use getJsonDesterilizationType()
            E json = new Gson().fromJson(reader, getJsonDeserializationType().getType());
            
            processJson(json);
        }
        catch(FileNotFoundException e) {
            Ainulindale.log.error("Unable to process the " + configName + " json file.");
            e.printStackTrace();
        }
        finally {
            IOUtils.closeQuietly(reader);
        }
    }
    
    public abstract void processJson(E json);
    
    public abstract TypeToken<E> getJsonDeserializationType();
}
