package eoa.ainulindale.common.config.json;

import java.util.List;
import com.google.gson.reflect.TypeToken;
import eoa.ainulindale.api.registries.WaypointRegistry;
import eoa.ainulindale.common.Ainulindale;

public class ConfigWaypointRegions extends JsonConfigBase<List<String>> {
    public ConfigWaypointRegions(WorldConfig config, String addonID) {
        super("waypointRegions", config, addonID);
    }

    @Override
    public void processJson(List<String> list) {
        if(list != null) {
            int i = 0;
            
            for(String region : list) {
                if(region.startsWith("example_")) continue;
                
                WaypointRegistry.registerRegion(NAME_SPACE, region);
                i++;
            }
            
            if(i > 0) Ainulindale.log.info("Registered " + i + " custom waypoint regions for addon "+NAME_SPACE+".");
        }
    }

    @Override
    public TypeToken<List<String>> getJsonDeserializationType() {
        return new TypeToken<List<String>>() {};
    }
}
