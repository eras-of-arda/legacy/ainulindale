package eoa.ainulindale.common.util.resources;

import lotr.common.LOTRTitle;

import java.util.UUID;

public class TitleProperties {
    public boolean isHidden;
    public LOTRTitle.TitleType titleType;
    public UUID[] uuids;
}
