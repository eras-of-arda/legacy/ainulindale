package eoa.ainulindale.common.util.resources;

import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.fac.LOTRControlZone;
import lotr.common.fac.LOTRFactionRank;
import lotr.common.fac.LOTRMapRegion;
import lotr.common.item.LOTRItemBanner;

import java.awt.*;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;

public class FactionProperties {
    public boolean allowPlayer;
    public boolean hasFixedAlignment;
    public int fixedAlignment;
    public LOTRDimension factionDimension;
    public LOTRDimension.DimensionRegion factionRegion;
    public List<LOTRControlZone> controlZones;
    public HashSet factionTypes;
    public String name;
    public Color color;
    public boolean allowEntityRegistry;
    public List<LOTRFactionRank> ranks;
    public LOTRFactionRank pledgeRank;
    public LOTRMapRegion mapInfo;
    public boolean isolationist;
    public boolean approvesOfWarCrimes;
    public List<LOTRItemBanner.BannerType> banners;
    public LOTRAchievement.Category category;

}
