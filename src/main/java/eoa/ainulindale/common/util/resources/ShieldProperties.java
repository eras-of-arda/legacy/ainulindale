package eoa.ainulindale.common.util.resources;

import lotr.common.LOTRShields;
import lotr.common.fac.LOTRFaction;
import net.minecraft.util.ResourceLocation;

import java.util.UUID;

public class ShieldProperties {
    public LOTRShields.ShieldType shieldType;
    public UUID[] exclusiveUUIDs;
    private LOTRFaction alignmentFaction;
    private boolean isHidden;
}
