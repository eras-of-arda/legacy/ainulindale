package eoa.ainulindale.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerAboutToStartEvent;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.api.registries.AchievementRegistry;
import eoa.ainulindale.api.registries.AddonRegistry;
import eoa.ainulindale.api.registries.BiomeRegistry;
import eoa.ainulindale.api.registries.FactionRegistry;
import eoa.ainulindale.api.registries.HillRegistry;
import eoa.ainulindale.api.registries.MapLabelRegistry;
import eoa.ainulindale.api.registries.MapRegistry;
import eoa.ainulindale.api.registries.RoadRegistry;
import eoa.ainulindale.api.registries.ShieldRegistry;
import eoa.ainulindale.api.registries.TitleRegistry;
import eoa.ainulindale.common.config.json.WorldConfig;
import eoa.ainulindale.common.network.AinulindaleNetworkHandler;
import eoa.ainulindale.common.world.biome.BiomeGenReplacementMeneltarma;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRDimension;
import lotr.common.LOTRShields;
import lotr.common.LOTRTitle;
import lotr.common.fac.LOTRFaction;
import lotr.common.fac.LOTRFactionRelations;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.map.LOTRMountains;
import lotr.common.world.map.LOTRRoads;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;

@Mod(modid = Ainulindale.MODID, version = Ainulindale.VERSION, name = Ainulindale.NAME, dependencies = "required-after:lotr", acceptableRemoteVersions = "*")
public class Ainulindale {
    public static final String MODID = "ainulindale";
    public static final String NAME = "Ainulindalė";
    public static final String VERSION = "0.0.2";

    public static final String API_NAME = "Ainulindalė-API";
    public static final String API_VERSION = "0.0.2";
    
    public static Logger log;

    public static boolean vanillaNeedsRefresh = true; //used to refresh vanilla on 1st load
    
    @SidedProxy(clientSide = "eoa.ainulindale.client.ClientProxy", serverSide = "eoa.ainulindale.common.CommonProxy")
    public static CommonProxy proxy;
    public static AinulindaleNetworkHandler networkHandler;
    
    public static File mainFolderPath;
    public static File EoAConfigFolderPath;
    public static WorldConfig worldConfig;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        log = event.getModLog();
        
        networkHandler = new AinulindaleNetworkHandler();

        AddonRegistry.registerAddon("lotr"); //this needs to be registered early

        mainFolderPath = event.getModConfigurationDirectory().getParentFile();
        EoAConfigFolderPath = new File(event.getModConfigurationDirectory(), "Ainulindalė");
        if(!Ainulindale.EoAConfigFolderPath.exists()) Ainulindale.EoAConfigFolderPath.mkdirs();

        proxy.preInit(event);
        
        LOTRDimension.MIDDLE_EARTH.biomeList[37] = null;
        LOTRDimension.MIDDLE_EARTH.colorsToBiomeIDs.remove(LOTRBiome.meneltarma.color);
        LOTRBiome.meneltarma = new BiomeGenReplacementMeneltarma(37, false).setTemperatureRainfall(0.9f, 0.8f).setMinMaxHeight(0.1f, 0.2f).setColor(9549658).setBiomeName("meneltarma");

        for(LOTRBiome biome : LOTRDimension.MIDDLE_EARTH.biomeList)
        {
            if(biome != null) {
                BiomeRegistry.setBiomeEntry("lotr", biome.biomeID, biome, biome.color);
            }
        }

        for(LOTRFaction fac : LOTRFaction.values())
        {
            FactionRegistry._addVanillaFaction(fac);
        }

        for(LOTRDimension.DimensionRegion region : LOTRDimension.DimensionRegion.values())
        {
            FactionRegistry.addFactionRegionAsObject("lotr", region.name(), region);
        }

        for(LOTRMountains hill : LOTRMountains.values())
        {
            HillRegistry.addHill("lotr", hill);
        }

        for(LOTRShields shield : LOTRShields.values())
        {
            ShieldRegistry.registerShield("lotr", shield);
        }


        worldConfig = new WorldConfig(new File(EoAConfigFolderPath, "custom-world"));
        worldConfig.initializeConfigsPreInit();


    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
        log.info("Trying to save roads");

        for(LOTRRoads road : LOTRRoads.getAllRoadsInWorld())
        {
            RoadRegistry.addRoad("lotr", ReflectionHelper.getPrivateValue(LOTRRoads.class, road, "roadName"), road);
        }


        log.info("Done trying to save roads");
    }

    @EventHandler
    public void postInit(FMLInitializationEvent event) {

        for(LOTRTitle title : LOTRTitle.allTitles)
        {
            TitleRegistry.registerTitle("lotr", title);
        }

        for(LOTRAchievement achievement : LOTRDimension.MIDDLE_EARTH.allAchievements)
        {
            AchievementRegistry.registerAchievement("lotr", achievement);
        }

        for(LOTRAchievement.Category achievementCategory : LOTRDimension.MIDDLE_EARTH.achievementCategories)
        {
            AchievementRegistry.registerCategory("lotr", achievementCategory);
        }

        for(LOTRMapLabels label : LOTRMapLabels.values())
        {
            MapLabelRegistry.registerMapLabel("lotr", label);
        }

        Map<LOTRFactionRelations.FactionPair, LOTRFactionRelations.Relation> relations = ReflectionHelper.getPrivateValue(LOTRFactionRelations.class, null, "defaultMap");

        for(LOTRFactionRelations.FactionPair pair : relations.keySet())
        {
            log.info("setting up vanilla relations for "+pair.getLeft().name()+" and "+pair.getRight().name());
            FactionRegistry._setFacRelationsVanilla(pair, relations.get(pair));
        }

        worldConfig.initializeConfigsPostInit();
        AddonRegistry.loadAddon("lotr");
    }
    
    @EventHandler
    public void serverAboutToStart(FMLServerAboutToStartEvent event) {
        MinecraftServer server = event.getServer();
        File datFile = new File(FMLCommonHandler.instance().getSavesDirectory(), server.getFolderName() + "/ainulindale_level.dat");
        
        System.out.println(server.getCurrentPlayerCount());
        
        try {
            //FileInputStream closed by readCompressed()
            NBTTagCompound levelDat = datFile.exists() ? CompressedStreamTools.readCompressed(new FileInputStream(datFile)) : new NBTTagCompound();
            
            if(levelDat.hasKey(MODID)) {
                NBTTagCompound tag = levelDat.getCompoundTag(MODID);
                String ID = tag.getString("AddonID");

                if(!AddonRegistry.getActiveAddon().equals(ID) || Ainulindale.vanillaNeedsRefresh) {
                    ResourceLocation foundID = null;
                    for(ResourceLocation res : MapRegistry.getRegisteredMaps().keySet()) {
                        if(res.toString().equals(AddonRegistry.getAddonProperties(ID).mapID.toString())) {
                            foundID = res;
                            break;
                        }
                    }

                    if(Ainulindale.vanillaNeedsRefresh)
                    {
                        log.info("Refreshing addons on first load.");
                        Ainulindale.log.info("ADDONLOAD 3b");
                        AddonRegistry.loadAddon(ID);
                        Ainulindale.vanillaNeedsRefresh = false;
                    }
                    else if(foundID != null) {
                        log.info("Loaded addon ID is " + AddonRegistry.getActiveAddon() + " server is set to " + ID + " switching.");
                        Ainulindale.log.info("ADDONLOAD 3");
                        AddonRegistry.loadAddon(ID);
                    }
                    else {
                        throw new RuntimeException("Tried starting server with addon ID " + ID + " but wasn't able to find ID.");
                    }
                }
            }
            else {
                datFile.createNewFile();
                NBTTagCompound tag = new NBTTagCompound();
                tag.setString("AddonID", AddonRegistry.getActiveAddon());
                levelDat.setTag(MODID, tag);
                CompressedStreamTools.writeCompressed(levelDat, new FileOutputStream(datFile));
            }
            
        }
        catch (IOException e) {
            log.error("Failed to read level.dat to check correct map type.");
        }
    }
}
