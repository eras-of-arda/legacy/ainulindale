package eoa.ainulindale.common.reflection;

import java.util.EnumSet;
import eoa.ainulindale.common.Ainulindale;
import lotr.client.gui.LOTRMapLabels;
import lotr.common.LOTRAchievement.Category;
import lotr.common.LOTRDimension;
import lotr.common.LOTRDimension.DimensionRegion;
import lotr.common.LOTRLore.LoreCategory;
import lotr.common.LOTRShields;
import lotr.common.LOTRShields.ShieldType;
import lotr.common.fac.LOTRFaction;
import lotr.common.fac.LOTRFaction.FactionType;
import lotr.common.fac.LOTRMapRegion;
import lotr.common.item.LOTRItemBanner.BannerType;
import lotr.common.quest.LOTRMiniQuestFactory;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.feature.LOTRTreeType;
import lotr.common.world.map.LOTRMountains;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.map.LOTRWaypoint.Region;
import lotr.common.world.spawning.LOTRInvasions;
import net.minecraftforge.common.util.EnumHelper;

public class LOTREnumHelpers {
    
    public static class WayPoints {
        public static LOTRWaypoint addWaypoint(String name, Region region, LOTRFaction faction, int x, int z, boolean hidden) {
            return addEnum(LOTRWaypoint.class, name, region, faction, x, z, hidden);
        }

        public static Region addWaypointRegion(String name) {
            return addEnum(Region.class, name);
        }
    }
    
    
    
    private static <T extends Enum<? >> T addEnum(Class<T> clazz, String name, Object... args) {
        Class<?>[] classArr = ReflectionUtils.getClassArr(args);
        
        return EnumHelper.addEnum(clazz, name, classArr, args);
    }
    
    
    

    public static LOTRMountains addMountain(String name, float x, float z, float h, int r) {
        return addMountain(name, x, z, h, r, 0);
    }

    public static LOTRMountains addMountain(String name, float x, float z, float h, int r, int lava) {
        Class<?>[] classArr = {float.class, float.class, float.class, int.class, int.class};
        Object[] args = {x, z, h, r, lava};

        return EnumHelper.addEnum(LOTRMountains.class, name, classArr, args);
    }

    public static BannerType addBannerType(String enumName, int id, String bannerName, LOTRFaction faction) {
        Class<?>[] classArr = {int.class, String.class, LOTRFaction.class};
        Object[] args = {id, bannerName, faction};

        BannerType banner = EnumHelper.addEnum(BannerType.class, enumName, classArr, args);
        LOTRReflectionHelpers.addBannerToIDMap(banner);
        return banner;
    }

    public static LOTRFaction addFaction(String enumName, int color, DimensionRegion region, EnumSet<FactionType> types) {
        return addFaction(enumName, color, LOTRDimension.MIDDLE_EARTH, region, true, true, Integer.MIN_VALUE, null, types);
    }

    public static LOTRFaction addFaction(String enumName, int color, LOTRDimension dim, LOTRDimension.DimensionRegion region, boolean player, boolean registry, int alignment, LOTRMapRegion mapInfo, EnumSet<FactionType> types) {
        Class<?>[] classArr = {int.class, LOTRDimension.class, DimensionRegion.class, boolean.class, boolean.class, int.class, LOTRMapRegion.class, EnumSet.class};
        Object[] args = {color, dim, region, player, registry, alignment, mapInfo, types};

        return EnumHelper.addEnum(LOTRFaction.class, enumName, classArr, args);
    }

    public static DimensionRegion addDimensionRegion(String enumName, String regionName) {
        Class<?>[] classArr = {String.class};
        Object[] args = {regionName};

        return EnumHelper.addEnum(DimensionRegion.class, enumName, classArr, args);
    }

    public static Category addAchievementCategory(String enumName, int color) {
        Class<?>[] classArr = {int.class};
        Object[] args = {color};

        return EnumHelper.addEnum(Category.class, enumName, classArr, args);
    }

    public static Category addAchievementCategory(String enumName, LOTRFaction faction) {
        Class<?>[] classArr = {LOTRFaction.class};
        Object[] args = {faction};

        return EnumHelper.addEnum(Category.class, enumName, classArr, args);
    }

    public static Category addAchievementCategory(String enumName, LOTRBiome biome) {
        Class<?>[] classArr = {LOTRBiome.class};
        Object[] args = {biome};

        return EnumHelper.addEnum(Category.class, enumName, classArr, args);
    }

    public static LOTRMapLabels addMapLabel(String enumName, LOTRBiome biomeLabel, int x, int y, float scale, int angle, float zoomMin, float zoomMan) {
        return addMapLabel(enumName, (Object) biomeLabel, x, y, scale, angle, zoomMin, zoomMan);
    }

    public static LOTRMapLabels addMapLabel(String enumName, String stringLabel, int x, int y, float scale, int angle, float zoomMin, float zoomMan) {
        return addMapLabel(enumName, (Object) stringLabel, x, y, scale, angle, zoomMin, zoomMan);
    }

    private static LOTRMapLabels addMapLabel(String enumName, Object label, int x, int y, float scale, int angle, float zoomMin, float zoomMan) {
        Class<?>[] classArr = {Object.class, int.class, int.class, float.class, int.class, float.class, float.class};
        Object[] args = {label, x, y, scale, angle, zoomMin, zoomMan};

        return EnumHelper.addEnum(LOTRMapLabels.class, enumName, classArr, args);
    }

    public static LOTRInvasions addInvasion(String enumName, LOTRFaction faction) {
        return addInvasion(enumName, faction, null);
    }

    public static LOTRInvasions addInvasion(String enumName, LOTRFaction faction, String subfaction) {
        Class<?>[] classArr = {LOTRFaction.class, String.class};
        Object[] args = {faction, subfaction};

        return EnumHelper.addEnum(LOTRInvasions.class, enumName, classArr, args);
    }

    public static LOTRShields addAlignmentShield(String enumName, LOTRFaction faction) {
        Class<?>[] classArr = {LOTRFaction.class};
        Object[] args = {faction};

        return EnumHelper.addEnum(LOTRShields.class, enumName, classArr, args);
    }

    public static LOTRShields addShield(String enumName, boolean hidden, String... players) {
        return addShield(enumName, ShieldType.EXCLUSIVE, hidden, players);
    }

    public static LOTRShields addShield(String enumName, ShieldType type, boolean hidden, String... players) {
        Class<?>[] classArr = {ShieldType.class, boolean.class, String[].class};
        Object[] args = {type, hidden, players};

        return EnumHelper.addEnum(LOTRShields.class, enumName, classArr, args);
    }

    public static LoreCategory addLoreCategory(String enumName, String name) {
        Class<?>[] classArr = {String.class};
        Object[] args = {name};

        return EnumHelper.addEnum(LoreCategory.class, enumName, classArr, args);
    }

    public static LOTRMiniQuestFactory addMiniQuestFactory(String enumName, String name) {
        Class<?>[] classArr = {String.class};
        Object[] args = {name};

        return EnumHelper.addEnum(LOTRMiniQuestFactory.class, enumName, classArr, args);
    }

    public static LOTRTreeType addTreeType(String enumName, Object treeFactory) {
        Class<?>[] classArr = null;
        try {
            classArr = new Class<?>[] {Class.forName("lotr.common.world.feature.LOTRTreeType$ITreeFactory")};
        }
        catch(ClassNotFoundException e) {
            Ainulindale.log.error("Failed to find the ITreeFactory interface.");
            e.printStackTrace();
        }
        Object[] args = {treeFactory};

        return EnumHelper.addEnum(LOTRTreeType.class, enumName, classArr, args);
    }
}
