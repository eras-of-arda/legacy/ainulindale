package eoa.ainulindale.common.reflection;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import eoa.ainulindale.api.Submod;
import net.minecraft.client.gui.GuiScreen;

public class CommonReflectionHelpers {
    
    /**
     * Gui related reflection helpers.
     */
    public static class Gui {
        /**
         * Create a new {@link GuiScreen} object for the given class and arguments.
         * 
         * @param the gui screen class
         * @param the constructor arguments
         */
        @SideOnly(value = Side.CLIENT)
        public static GuiScreen getGuiScreen(Class<? extends GuiScreen> clazz, Object[] args) {
            return ReflectionUtils.Constructors.invokeConstructor(clazz, args);
        }
    }
    
    public static class Submods {
        public static Submod createSubmod(String clazz) {
            return ReflectionUtils.Constructors.invokeConstructor(clazz);
        }
    }
}
