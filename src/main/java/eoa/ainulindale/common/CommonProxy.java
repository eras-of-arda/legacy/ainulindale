package eoa.ainulindale.common;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {
    protected CommonTickHandler tickHandler;
    protected CommonEventHandler eventHandler;
    protected AinulindaleEventHandler ainulindaleEventHandler;


    public void preInit(FMLPreInitializationEvent event) {
        registerSidedHandlers();
        registerHandlers();
        
    }

    public void init(FMLInitializationEvent event) {
    }

    public void postInit(FMLPostInitializationEvent event) {
    }
    
    public void registerSidedHandlers() {
        tickHandler = new CommonTickHandler();
        eventHandler = new CommonEventHandler();
    }
    
    public void registerHandlers() {
        ainulindaleEventHandler = new AinulindaleEventHandler();
    }
}
