package eoa.ainulindale.core;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import cpw.mods.fml.relauncher.FMLLaunchHandler;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.core.hooks.PreMCHooks;
import eoa.ainulindale.core.patches.base.Patcher;
import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.LaunchClassLoader;

public class AinulindaleClassTransformer implements IClassTransformer {

    static {
        FMLLaunchHandler launchHandler = ReflectionHelper.getPrivateValue(FMLLaunchHandler.class, null, "INSTANCE");
        LaunchClassLoader classLoader = ReflectionHelper.getPrivateValue(FMLLaunchHandler.class, launchHandler, "classLoader");
        
        PreMCHooks.transformerExclusionsTweaks(classLoader);
    }

    @Override
    public byte[] transform(String name, String transformedName, byte[] classBytes) {
        boolean ran = false;

        for(Patcher patcher : AinulindaleCoreMod.activePatches) {
            if(patcher.canRun(name)) {
                ran = true;

                ClassNode classNode = new ClassNode();
                ClassReader classReader = new ClassReader(classBytes);
                classReader.accept(classNode, 0);

                AinulindaleCoreMod.log.info("Running patcher " + patcher.getName() + " for " + name);
                patcher.run(name, classNode);

                ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
                classNode.accept(writer);
                classBytes = writer.toByteArray();
            }
        }

        if(ran) {
            AinulindaleCoreMod.activePatches.removeIf(patcher -> patcher.isDone());
            if(AinulindaleCoreMod.activePatches.isEmpty()) AinulindaleCoreMod.log.info("Ran all active patches.");
        }

        return classBytes;
    }
}
