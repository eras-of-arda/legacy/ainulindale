package eoa.ainulindale.core.hooks;

import java.util.Set;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.core.AinulindaleCoreMod;
import net.minecraft.launchwrapper.LaunchClassLoader;

public class PreMCHooks {
    public static void postFMLLoad() {
        AinulindaleCoreMod.loadModPatches();
    }
    
    public static void transformerExclusionsTweaks(LaunchClassLoader classLoader) {
        Set<String> exclusions = ReflectionHelper.getPrivateValue(LaunchClassLoader.class, classLoader, "transformerExceptions");

        exclusions.remove("lotr");
        exclusions.add("lotr.common.coremod");
    }
}
