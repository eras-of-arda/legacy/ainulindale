package eoa.ainulindale.api.registries;

import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.reflection.LOTREnumHelpers;
import eoa.ainulindale.common.util.resources.AchievementProperties;
import lotr.common.LOTRAchievement;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The achievement registry.
 * <p>
 * Use this class to register and or get any {@link LOTRAchievement}. When adding or removing an achievement you should always use this class instead of directly using {@link LOTRAchievement}.
 */
public class AchievementRegistry {
    static Map<String, List<LOTRAchievement>> achieves = new HashMap<String, List<LOTRAchievement>>();
    static Map<String, List<LOTRAchievement.Category>> categories = new HashMap<String, List<LOTRAchievement.Category>>();
    static Map<LOTRAchievement, AchievementProperties> achieveData = new HashMap<LOTRAchievement, AchievementProperties>();

    /**
     * Creates an registers an {@link LOTRAchievement.Category} for the given addon
     *
     * @param addonID the addon ID
     * @param category the category to put the achievement
     * @param index the index of the achievement in the category
     * @param iconItem the item to use as the achievement icon
     * @param name the name of the achievement
     */
    public static void addAchievement(String addonID, LOTRAchievement.Category category, int index, ItemStack iconItem, String name)
    {
        LOTRAchievement theAchieve = new LOTRAchievement(category, index, iconItem, name);
        if(!achieves.containsKey(addonID)) achieves.put(addonID, new ArrayList<LOTRAchievement>());
        achieves.get(addonID).add(theAchieve);
    }

    /**
     * Registers an {@link LOTRAchievement.Category} for the given addon
     *
     * @param addonID the addon ID
     * @param achievement the achievement to register
     */
    public static void registerAchievement(String addonID, LOTRAchievement achievement)
    {
        if(!achieves.containsKey(addonID)) achieves.put(addonID, new ArrayList<LOTRAchievement>());
        achieves.get(addonID).add(achievement);
    }

    static void hideAchievement(String addonID, LOTRAchievement achievement)
    {
        AchievementProperties props = new AchievementProperties();
        props.category = achievement.category;
        achievement.category.list.remove(achievement);
        achieveData.put(achievement, props);
        achievement.category.dimension.allAchievements.remove(achievement);
        if(achievement.getAchievementTitle() != null){
            TitleRegistry.hideTitle(achievement.getAchievementTitle());
        }
    }


    static void showAchievement(String addonID, LOTRAchievement achievement)
    {
        AchievementProperties props = achieveData.get(achievement);
        achievement.category = props.category;
        achievement.category.list.add(achievement);
        achievement.category.dimension.allAchievements.add(achievement);
        if(achievement.getAchievementTitle() != null){
            TitleRegistry.showTitle(achievement.getAchievementTitle());
        }
    }

    /**
     * Creates a new {@link LOTRAchievement.Category} for the given addon
     *
     * @param addonID the addon ID
     * @param name the name of the new achievement category
     * @param color the color for the new achievement category
     */
    public static void addCategory(String addonID, String name, int color)
    {
        String namespacedName = addonID+":"+name;
        LOTRAchievement.Category category = LOTREnumHelpers.addAchievementCategory(namespacedName, color);
        if(!categories.containsKey(addonID)) categories.put(addonID, new ArrayList<LOTRAchievement.Category>());
        categories.get(addonID).add(category);
    }

    /**
     * Gets a given {@link LOTRAchievement.Category} for the given addon
     *
     * @param addonID the addon ID
     * @param name the name of the achievement category to get
     * @return the {@link LOTRAchievement.Category} object for the category
     */
    public static LOTRAchievement.Category getCategory(String addonID, String name)
    {
        String namespacedName = addonID+":"+name;
        return LOTRAchievement.Category.valueOf(namespacedName);
    }

    /**
     * Registers an {@link LOTRAchievement.Category} for the given addon
     *
     * @param addonID the addon ID
     * @param category the category to register
     */
    public static void registerCategory(String addonID, LOTRAchievement.Category category)
    {
        Ainulindale.log.info("Registering category "+category.name()+" for addon "+addonID);
        if(!categories.containsKey(addonID)) categories.put(addonID, new ArrayList<LOTRAchievement.Category>());
        categories.get(addonID).add(category);
    }

    static void hideCategory(String addonID, LOTRAchievement.Category category)
    {
        Ainulindale.log.info("Hiding category "+category.name()+" for addon "+addonID);
        AchievementProperties props = new AchievementProperties();
        category.dimension.achievementCategories.remove(category);
    }

    static void showCategory(String addonID, LOTRAchievement.Category category)
    {
        Ainulindale.log.info("Showing category "+category.name()+" for addon "+addonID);
        category.dimension.achievementCategories.add(category);
    }

    /**
     * Sets the list of achievements for the given addon
     *
     * @param addonID the addon ID
     * @return the list of achievements
     */
    public static List<LOTRAchievement> getAchievementList(String addonID)
    {
        if(!achieves.containsKey(addonID)) achieves.put(addonID, new ArrayList<LOTRAchievement>());
        return achieves.get(addonID);
    }

    /**
     * Sets the list of achievement categories for the given addon
     *
     * @param addonID the addon ID
     * @return the list of achievement categories for the addon
     */
    public static List<LOTRAchievement.Category> getCategoryList(String addonID)
    {
        if(!categories.containsKey(addonID)) categories.put(addonID, new ArrayList<LOTRAchievement.Category>());
        return categories.get(addonID);
    }


}
