package eoa.ainulindale.api.registries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import lotr.common.world.map.LOTRRoads;

public class RoadRegistry {
    static Map<String, List<LOTRRoads>> roadsList = new HashMap<String, List<LOTRRoads>>();
    static Map<String, Map<LOTRRoads, LOTRRoads.RoadPoint[]>> roadPointsLists = new HashMap<String, Map<LOTRRoads, LOTRRoads.RoadPoint[]>>();

    /**
     * Creates a new {@link LOTRRoads} instance for the given addon ID
     * @param addonID the addon ID
     * @param roadName the name of the new road
     * @param points the {@link LOTRRoads.RoadPoint} instances making up the road
     * @return the list of {@link LOTRRoads} objects making up the new road
     */
    public static List<LOTRRoads> createRoad(String addonID, String roadName, Object... points)
    {

        Ainulindale.log.info("adding road "+roadName+" for addon "+addonID);

        List<LOTRRoads> roads = new ArrayList<LOTRRoads>();

        List<LOTRRoads.RoadPoint> pointsFinal = new ArrayList<LOTRRoads.RoadPoint>();

        LOTRReflectionHelpers.registerRoad(addonID+":"+roadName, points);
        for(LOTRRoads road : LOTRRoads.getAllRoadsInWorld())
        {
            //Ainulindale.log.info("testing road "+ReflectionHelper.getPrivateValue(LOTRRoads.class, road, "roadName"));
            if(ReflectionHelper.getPrivateValue(LOTRRoads.class, road, "roadName").equals(addonID+":"+roadName))
            {
                //Ainulindale.log.info("found road "+ReflectionHelper.getPrivateValue(LOTRRoads.class, road, "roadName"));
                if(!roadsList.containsKey(addonID)) roadsList.put(addonID, new ArrayList<LOTRRoads>());
                roadsList.get(addonID).add(road);
                if(!roadPointsLists.containsKey(addonID)) roadPointsLists.put(addonID, new HashMap<LOTRRoads, LOTRRoads.RoadPoint[]>());
                roadPointsLists.get(addonID).put(road, road.roadPoints);
            }
        }
        return roads;
    }

    /**
     * Gets the list {@link LOTRRoads} for the given addon ID
     * @param addonID the addon ID
     * @return the list of roads
     */
    public static List<LOTRRoads> getRoadsList(String addonID)
    {
        if(!roadsList.containsKey(addonID)) roadsList.put(addonID, new ArrayList<LOTRRoads>());
        return roadsList.get(addonID);
    }

    /**
     * Registers the given {@link LOTRRoads} instance for the given addon ID with the given name
     * @param addonID the addon ID
     * @param roadName the name of the road to register
     * @param road the road to register
     */
    public static LOTRRoads addRoad(String addonID, String roadName, LOTRRoads road)
    {
        Ainulindale.log.info("Saving road "+roadName+" for "+addonID);
        if(!roadsList.containsKey(addonID)) roadsList.put(addonID, new ArrayList<LOTRRoads>());
        roadsList.get(addonID).add(road);
        roadPointsLists.get(addonID).put(road, road.roadPoints);
        return road;
    }

    /**
     * Registers the given {@link LOTRRoads} instance for the given addon ID
     * @param addonID the addon ID
     * @param road the road to register
     */
    public static LOTRRoads addRoad(String addonID, LOTRRoads road)
    {
        return addRoad(addonID, road.getDisplayName(), road);
    }

    /**
     * Unregisters a {@link LOTRRoads} instance for the given addon
     * @param addonID the addon ID
     * @param name the name of the road to unregister
     */
    public static void removeRoad(String addonID, String name)
    {
        String namespacedName;
        if(addonID == "lotr")
        {
            namespacedName = addonID+":"+name;
        }
        else
        {
            namespacedName = name;
        }
        int i = 0;
        for(LOTRRoads road : roadsList.get(addonID))
        {
            if(ReflectionHelper.getPrivateValue(LOTRRoads.class, road, "roadName") == namespacedName)
            {
                roadsList.get(addonID).remove(i);
                if(AddonRegistry.getActiveAddon() == addonID)
                {
                    LOTRRoads.getAllRoadsInWorld().remove(road);
                }
            }
            i++;
        }
    }

    /**
     * Hides the given {@link LOTRRoads} instance for the current active addon
     * @param addonID the addon ID
     * @param road the road to hide
     */
    static void saveRoad(String addonID, LOTRRoads road)
    {
        Ainulindale.log.info("disabling road "+road.getDisplayName()+" for addon "+addonID);
        //if(!roadPointsLists.containsKey(addonID)) roadPointsLists.put(addonID, new HashMap<LOTRRoads, LOTRRoads.RoadPoint[]>());
       // roadPointsLists.get(addonID).put(road, road.roadPoints);
        //if(roadsList.containsKey(addonID) && !roadsList.get(addonID).contains(road))
        //{
        //    roadsList.get(addonID).add(road);
        //}
        LOTRRoads.getAllRoadsInWorld().remove(road);
    }

    /**
     * Shows the given {@link LOTRRoads} instance for the current active addon
     * @param addonID the addon ID
     * @param road the road to show
     */
    static void restoreRoad(String addonID, LOTRRoads road)
    {
        Ainulindale.log.info("enabling road "+road.getDisplayName()+", with "+roadPointsLists.get(addonID).get(road).length+" points for addon "+addonID);
        LOTRRoads.getAllRoadsInWorld().add(road);
        road.roadPoints = roadPointsLists.get(addonID).get(road);
    }
}
