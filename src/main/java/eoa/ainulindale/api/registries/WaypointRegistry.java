package eoa.ainulindale.api.registries;

import java.util.*;

import cpw.mods.fml.relauncher.ReflectionHelper;
import eoa.ainulindale.api.event.registry.WaypointRegistryEvent;
import eoa.ainulindale.common.Ainulindale;
import eoa.ainulindale.common.reflection.LOTREnumHelpers;
import eoa.ainulindale.common.reflection.LOTRReflectionHelpers;
import eoa.ainulindale.common.util.resources.WaypointProperties;
import lotr.common.LOTRModInfo;
import lotr.common.fac.LOTRFaction;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.map.LOTRWaypoint.Region;

/**
 * The waypoint registry.
 * <p>
 * Use this class to register and or get any {@link LOTRWaypoint}. When accessing a waypoint you should always use this class instead of directly using {@link LOTRWaypoint}.
 */
public class WaypointRegistry {
    static Map<String, List<LOTRWaypoint>> waypoints = new HashMap<String, List<LOTRWaypoint>>();
    static Map<String, List<Region>> regions = new HashMap<String, List<Region>>();
    public static Map<LOTRWaypoint, WaypointProperties> wpData = new HashMap<LOTRWaypoint, WaypointProperties>();
    

    /**
     * Register a new {@link Region} to the region registry. Usually this method will be called in the {@link WaypointRegistryEvent}.
     * 
     * @param nameSpace the namespace you want to register it to, usually the same as your mod id.
     * @param name the waypoint enum name
     * @return the newly registed region.
     */
    public static Region registerRegion(String nameSpace, String name) {
        Region region = LOTREnumHelpers.WayPoints.addWaypointRegion(name);
        
        if(!regions.containsKey(nameSpace)) regions.put(nameSpace, new ArrayList<Region>());
        regions.get(nameSpace).add(region);
        
        return region;
    }
    
    /**
     * Create and register a new {@link LOTRWaypoint} to the waypoint registry.
     * Calls {@link #addWaypoint(String, String, Region, LOTRFaction, int, int, boolean)}
     * 
     * @param nameSpace the namespace you want to register it to, usually the same as your mod id.
     * @param name the waypoint enum name
     * @param region the waypoint region
     * @param faction the waypoint faction
     * @param x the waypoint x image coordinate
     * @param y the waypoint y image coordinate
     * @return the newly registered waypoint
     */
    public static LOTRWaypoint addWaypoint(String nameSpace, String name, LOTRWaypoint.Region region, LOTRFaction faction, int x, int y) {
        return addWaypoint(nameSpace, name, region, faction, x, y, false);
    }
    
    /**
     * Create and register a new {@link LOTRWaypoint} to the waypoint registry.
     * 
     * @param nameSpace the namespace you want to register it to, usually the same as your mod id.
     * @param name the waypoint enum name
     * @param region the waypoint region
     * @param faction the waypoint faction
     * @param x the waypoint x image coordinate
     * @param y the waypoint y image coordinate
     * @param hidden if the waypoint is a hidden waypoint or not
     * @return the newly registed waypoint
     */
    public static LOTRWaypoint addWaypoint(String nameSpace, String name, LOTRWaypoint.Region region, LOTRFaction faction, int x, int y, boolean hidden) {
        LOTRWaypoint wp = LOTREnumHelpers.WayPoints.addWaypoint(nameSpace+":"+name, region, faction, x, y, hidden);
        Ainulindale.log.info("Creating WP "+wp.name()+"- hidden: "+hidden+", fac: "+faction.name());
        if(!waypoints.containsKey(nameSpace)) waypoints.put(nameSpace, new ArrayList<LOTRWaypoint>());
        waypoints.get(nameSpace).add(wp);
        
        return wp;
    }


    /**
     * Creates an empty waypoint list in the registry for an addon
     *
     * @param nameSpace the addon ID
     */
    public static void createWaypointList(String nameSpace)
    {
        if(!waypoints.containsKey(nameSpace)) waypoints.put(nameSpace, new ArrayList<LOTRWaypoint>());
    }

    /**
     * Registers an existing {@link LOTRWaypoint} to the waypoint registry
     *
     * @param nameSpace the addon ID
     * @param wp the waypoint
     * @return the waypoint passed in
     */
    public static LOTRWaypoint registerWaypointWithoutAdding(String nameSpace, LOTRWaypoint wp) {

        if(!waypoints.containsKey(nameSpace)) waypoints.put(nameSpace, new ArrayList<LOTRWaypoint>());
        waypoints.get(nameSpace).add(wp);

        return wp;
    }
    
    /**
     * Get all {@link LOTRWaypoint} in the {@code nameSpace}.
     * 
     * @param nameSpace
     * @return a list with all the waypoints or null if the namespace doesn't exist.
     */
    public static List<LOTRWaypoint> getWaypoints(String nameSpace) {
        return waypoints.get(nameSpace);
    }

    /**
     * Get a {@link LOTRWaypoint} by enum{@code Name}.
     * 
     * @param name the enum name
     * @return the waypoint or null if no waypoint if no matching waypoint is found
     */
    public static LOTRWaypoint getWaypoint(String name) {
        return LOTRWaypoint.waypointForName(name);
    }

    /**
     * Get a {@link LOTRWaypoint} by addon ID and WP name
     * @param nameSpace the addon ID
     * @param name the WP name
     * @return the waypoint or null if no waypoint if no matching waypoint is found
     */
    public static LOTRWaypoint getWaypoint(String nameSpace, String name) {
        return LOTRWaypoint.waypointForName(nameSpace+":"+name);
    }
    
    /**
     * Get all {@link Region} in the {@code nameSpace}.
     * 
     * @param nameSpace
     * @return a list with all the regions or null if the namespace doesn't exist.
     */
    public static List<Region> getRegions(String nameSpace) {
        return regions.get(nameSpace);
    }

    /**
     * Get a {@link Region} by enum{@code Name}.
     * 
     * @param name the enum name
     * @return the waypoint or null if no waypoint if no matching waypoint is found
     */
    public static Region getRegion(String name) {
        return LOTRWaypoint.regionForName(name);
    }


    static void clearWP(LOTRWaypoint wp)
    {
        if(!wpData.containsKey(wp)) {
            WaypointProperties props = new WaypointProperties();
            props.fac = wp.faction;
            props.hidden = wp.isHidden();
            wpData.put(wp, props);
        }
        LOTRReflectionHelpers.disableWaypoint(wp);
    }

    /**
     * Creates an empty waypoint list in the registry for an addon
     *
     * @param wp the addon ID
     */
    static void activateWP(LOTRWaypoint wp)
    {
        WaypointProperties props = wpData.get(wp);
        Ainulindale.log.info("Activating WP "+wp.name()+"- hidden: "+props.hidden+", fac: "+props.fac.name());
        wp.faction = props.fac;
        ReflectionHelper.setPrivateValue(LOTRWaypoint.class, wp, props.hidden, "isHidden");
    }
    
    static {
        LOTRWaypoint[] lotrWaypoints = LOTRWaypoint.values();
        waypoints.put(LOTRModInfo.modID, new ArrayList<LOTRWaypoint>(Arrays.asList(lotrWaypoints)));
        
        Region[] lotrRegions = LOTRWaypoint.Region.values();
        regions.put(LOTRModInfo.modID, new ArrayList<Region>(Arrays.asList(lotrRegions)));
    }
}
