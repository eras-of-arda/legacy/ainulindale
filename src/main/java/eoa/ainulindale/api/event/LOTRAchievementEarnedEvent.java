package eoa.ainulindale.api.event;

import eoa.ainulindale.api.event.registry.LOTREvent;
import lotr.common.LOTRAchievement;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.PlayerEvent;

/**
 * When the player receives an lotr achievement. TODO If canceled the player will not receive anything.
 */
public class LOTRAchievementEarnedEvent extends PlayerEvent implements LOTREvent {
    
    public final LOTRAchievement achievement;
    public final boolean display; //TODO add display
    
    public LOTRAchievementEarnedEvent(EntityPlayer player, LOTRAchievement achievement) {
        super(player);
        this.achievement = achievement;
        display = false;
    }
}
