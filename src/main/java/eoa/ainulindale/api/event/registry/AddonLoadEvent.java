package eoa.ainulindale.api.event.registry;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.eventhandler.Event;
import eoa.ainulindale.api.AinulindaleAPI;
import eoa.ainulindale.api.registries.WaypointRegistry;
import lotr.common.world.map.LOTRWaypoint;
import lotr.common.world.map.LOTRWaypoint.Region;

/**
 * The addon load event. This event is called on the {@link AinulindaleAPI#LOTR_EVENT_BUS}. During this event you should do any setup for your addon not handled directly by Ainulindalë (e.g. changing alignment icon, messing with the Star of Eärendil, etc.)
 * <p>
 * The event itself will be called upon addon loading, right after loadAddon runs.
 */
public class AddonLoadEvent extends Event implements LOTREvent {

    public String addonID;

    public AddonLoadEvent(String addonID)
    {
        this.addonID = addonID;
    }

}
