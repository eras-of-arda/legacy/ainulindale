package eoa.ainulindale.client.gui.button;

import eoa.ainulindale.common.reflection.CommonReflectionHelpers;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class GuiButtonOpenGui extends GuiButtonAction {
	private Class<? extends GuiScreen> screenClass;
	private Object[] args;
	private Minecraft mc;

	public GuiButtonOpenGui(int id, int x, int y, int w, int h, String s, Minecraft mc, Class<? extends GuiScreen> screenClass) {
        this(id, x, y, w, h, s, mc, screenClass, new Object[0]);
    }
	
    public GuiButtonOpenGui(int id, int x, int y, int w, int h, String s, Minecraft mc, Class<? extends GuiScreen> screenClass, Object[] args) {
        super(id, x, y, w, h, s, null);
        this.screenClass = screenClass;
        this.args = args;
        this.mc = mc;
    }

    public void openGui() {
    	this.mc.displayGuiScreen(CommonReflectionHelpers.Gui.getGuiScreen(screenClass, args));
    }
}
