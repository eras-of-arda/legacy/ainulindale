package eoa.ainulindale.client.gui.button;

import java.util.List;
import org.lwjgl.opengl.GL11;
import lotr.client.gui.LOTRGuiRedBook;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.StatCollector;
import net.minecraft.util.StringUtils;

public class GuiButtonAction extends GuiButton {
    protected String baseDisplayString;
    protected String baseDescString;
    private boolean redBookRender = false;

    public GuiButtonAction(int id, int x, int y, int w, int h, String s) {
        this(id, x, y, w, h, s, null);
    }
    
    public GuiButtonAction(int id, int x, int y, int w, int h, String s, String desc) {
        super(id, x, y, w, h, StatCollector.translateToLocal(s));
        this.baseDisplayString = s;
        this.baseDescString = desc;
    }

    protected String getDescription() {
        if(baseDescString != null) return StatCollector.translateToLocal(this.baseDescString + ".desc");
        if(baseDisplayString != null) return StatCollector.translateToLocal(this.baseDisplayString + ".desc");
        
        return null;
    }
    
    public GuiButtonAction setRedBookRender(boolean b) {
        redBookRender = b;
        return this;
    }

    public void drawTooltip(GuiScreen screen, int i, int j) {
        if(this.enabled && this.func_146115_a()) {
        	FontRenderer renderer = screen.mc.fontRenderer;
            String desc = getDescription();
            
            if(StringUtils.isNullOrEmpty(desc)) return;
            
            int border = 3;
            int offset = 10;

            i += offset;
            j += offset;
            int stringWidth = Math.min(screen.width - i - 2 * border, 200);
            
            List<String> splitStrings = renderer.listFormattedStringToWidth(desc, stringWidth);
            
            int stringHeight = splitStrings.size() * renderer.FONT_HEIGHT;
            stringWidth = 0;
            for(String string : splitStrings) {
            	stringWidth = Math.max(stringWidth, renderer.getStringWidth(string));
            }
            
            Gui.drawRect(i , j, i + stringWidth + border * 2, j + stringHeight + border * 2, -1073741824);
            renderer.drawSplitString(desc, i + border, j + border, stringWidth, 0xFFFFFF);
        }
    }
    
    @Override
    public void drawButton(Minecraft mc, int i, int j) {
        if(this.visible) {
            if(redBookRender) {
                FontRenderer fontrenderer = mc.fontRenderer;
                mc.getTextureManager().bindTexture(LOTRGuiRedBook.guiTexture);
                GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                this.field_146123_n = i >= this.xPosition && j >= this.yPosition && i < this.xPosition + this.width && j < this.yPosition + this.height;
                int k = this.getHoverState(this.field_146123_n);
                Gui.func_146110_a(this.xPosition, this.yPosition, 170.0f, 256 + k * 20, this.width, this.height, 512.0f, 512.0f);
                GL11.glEnable(3042);
                GL11.glBlendFunc(770, 771);
                Gui.func_146110_a(this.xPosition, this.yPosition, 170.0f, 316.0f, this.width / 2, this.height, 512.0f, 512.0f);
                Gui.func_146110_a(this.xPosition + this.width / 2, this.yPosition, 370 - this.width / 2, 316.0f, this.width / 2, this.height, 512.0f, 512.0f);
                this.mouseDragged(mc, i, j);
                int color = 8019267;
                if(!this.enabled) {
                    color = 5521198;
                }
                else if(this.field_146123_n) {
                    color = 8019267;
                }
                fontrenderer.drawString(this.displayString, this.xPosition + this.width / 2 - fontrenderer.getStringWidth(this.displayString) / 2, this.yPosition + (this.height - 8) / 2, color);
            }
            else {
                super.drawButton(mc, i, j);
            }
        }
    }
}
